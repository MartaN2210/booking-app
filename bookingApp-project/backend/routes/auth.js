const router = require('express').Router();
const bodyParser = require('body-parser');
require('dotenv').config(); // to use .env variables
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

const saltRounds = 10;

const requireAuth = (req, res, next) => {
  const { userId } = req.session;

  if (!userId) {
    return res.status(401).json({ message: 'Not auth' });
  }
  next();
};

router.post('/auth/login', (req, res) => {
  const { email, password } = req.body;
  const str = `SELECT * FROM users WHERE user_email=?`;

  if (email && password) {
    connection.query(str, [email], (error, results) => {
      if (error) throw error;
      const [user] = results;
      if (user) {
        bcrypt.compare(password, user.password, (err, result) => {
          if (result) {
            req.session.userId = user.user_id;
            req.session.userRole = user.user_role;
            req.session.user = user;
            console.log(req.session.userId);
            res.send(user);
          } else {
            req.session.destroy();
            res.status(403).send({ message: `Wrong user name or password` });
          }
        });
      }
    });
  } else {
    res.status(501).send({ message: `All fields are required` });
  }
});

function validateNewUser(req, res, next) {
  const { email } = req.body;
  const strSelect = `SELECT * FROM users WHERE user_email=?`;

  connection.query(strSelect, [email], (error, results) => {
    if (error) throw error;
    const [user] = results;
    if (user) {
      res.status(400).send({ message: `User already exists` });
    } else {
      next();
    }
  });
}

router.post('/auth/signup', validateNewUser, (req, res) => {
  const { email, password } = req.body;
  const strToInsert = `INSERT INTO users (user_email, password) VALUES (? ,?)`;

  bcrypt.hash(password, saltRounds).then((hashPassword) => {
    connection.query(strToInsert, [email, hashPassword], (error) => {
      if (error) throw error;
      res.status(200).send({ message: `User created successfully` });
    });
  });
});

router.post('/auth/new_password', (req, res) => {
  const { password } = req.body;
  const email = req.session.userEmail;
  const sqlUpdateUser = `UPDATE users SET password = ? WHERE user_email = ?`;

  bcrypt.hash(password, saltRounds).then((hashPassword) => {
    connection.query(sqlUpdateUser, [hashPassword, email], (error) => {
      if (error) throw error;
      res.status(200).send({ message: `User updated successfully` });
    });
  });
});

router.post('/auth/add_user', requireAuth, validateNewUser, (req, res) => {
  const timestamp = Date.now();

  const user = {
    email: req.body.email,
    timestamp: timestamp,
    token_number: '',
    confirmed: 0,
  };

  const token = jwt.sign({ user }, process.env.JWT_ACC_ACTIVATE, {
    expiresIn: '20m',
  });

  user.token_number = token;

  const str = `INSERT INTO email_verifications (user_email, token_number, timestamp, confirmed) VALUES (?, ?, ?, ?); INSERT INTO users (user_email) VALUES (?)`;

  connection.query(
    str,
    [user.email, user.token_number, timestamp, user.confirmed, user.email],
    function (err, result) {
      if (err) throw err;
    }
  );

  nodemailer.createTestAccount((err, _account) => {
    const htmlEmail = `<h3> Please click and follow the link to create your account:
      <a href='${process.env.BACKEND_URL}/auth/email_verification/${token}'>${process.env.BACKEND_URL}/auth/email_verification/${token}</a>
      </h3>`;
    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: 'lydia64@ethereal.email',
        pass: 'pjRP8TaYaSx9FWyuQe',
      },
    });
    const mailOptions = {
      from: 'test@test-account.com',
      to: 'glenda.nikolaus46@ethereal.email',
      replyTo: 'test@test-account.com',
      subject: 'Confirm Email',
      text: 'sth',
      html: htmlEmail,
    };

    transporter.sendMail(mailOptions, (err, _info) => {
      if (err) {
        console.log(err);
      }
    });
  });
});

router.get('/auth/email_verification/:token', (req, res) => {
  const { token } = req.params;
  if (token) {
    jwt.verify(
      token,
      process.env.JWT_ACC_ACTIVATE,
      function (err, decodedToken) {
        if (err) {
          return res.status(400).json({ error: 'Incorrect or expired link. ' });
        }
        const passedUserData = decodedToken;
        passedUserData.user.tokenNumber = token;
        passedUserData.user.confirmed = 1;
        const { tokenNumber } = passedUserData.user;
        const { confirmed } = passedUserData.user;
        if (
          passedUserData.user.email &&
          tokenNumber &&
          passedUserData.user.timestamp &&
          confirmed
        ) {
          const updateConfirmed = 1;
          const str = `UPDATE email_verifications SET confirmed = ? WHERE user_email = ? AND token_number= ? AND timestamp= ?`;
          connection.query(
            str,
            [
              updateConfirmed,
              passedUserData.user.email,
              tokenNumber,
              passedUserData.user.timestamp,
            ],
            function (error, results, fields) {
              if (!results) {
                return res
                  .status(501)
                  .send({ message: `Something went wrong` });
              }
              req.session.userEmail = passedUserData.user.email;
              return res.redirect('http://localhost:3000/new_password');
            }
          );
        }
      }
    );
  } else {
    return res.json({ error: 'Something went wrong' });
  }
});

router.get('/auth/is_auth', (req, res) => {
  if (!req.session.user) {
    res.send({ auth: false });
  } else {
    res.send(req.session.user);
  }
});

router.get('/auth/logout', (req, res) => {
  req.session.destroy();
  res.send({ auth: false });
});

module.exports = {
  router,
  requireAuth,
};
