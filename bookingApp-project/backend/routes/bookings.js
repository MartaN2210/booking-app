const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth.js');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(authRoutes.requireAuth);

// get all bookings

router.get('/bookings', (req, res) => {
  const str = `SELECT bookings.booking_id, bookings.date, bookings.time, bookings.facility_id, users.user_email FROM bookings INNER JOIN users ON bookings.user_id = users.user_id`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get all bookings filtred

router.get('/bookings/filter', (req, res) => {
  if ((req.query.from_date && req.query.to_date, req.query.facility_id)) {
    const sqlSelect = `SELECT * FROM bookings WHERE date >= ? and date <= ? and facility_id = ?`;

    connection.query(
      sqlSelect,
      [req.query.from_date, req.query.to_date, req.query.facility_id],
      (error, results) => {
        if (error) throw error;
        const bookings = results;
        return res.send({ data: bookings });
      }
    );
  }
  if (req.query.from_date && req.query.user_id) {
    const sqlSelect = `SELECT * FROM bookings LEFT JOIN facilities ON bookings.facility_id=facilities.facility_id WHERE bookings.date >= ? AND bookings.user_id = ?`;

    connection.query(
      sqlSelect,
      [req.query.from_date, req.query.user_id],
      (error, results) => {
        if (error) throw error;
        const bookings = results;
        return res.send({ data: bookings });
      }
    );
  }
  if (req.query.to_date && req.query.user_id) {
    const sqlSelect = `SELECT * FROM bookings LEFT JOIN facilities ON bookings.facility_id=facilities.facility_id WHERE bookings.date < ? AND bookings.user_id = ?`;

    connection.query(
      sqlSelect,
      [req.query.to_date, req.query.user_id],
      (error, results) => {
        if (error) throw error;
        const bookings = results;
        return res.send({ data: bookings });
      }
    );
  }
});

// add a new booking

router.post('/bookings', (req, res) => {
  const { checkedTimeSlots } = req.body;
  const { chosenDate } = req.body;
  const { chosenFacility } = req.body;
  let sqlInsert = ``;

  checkedTimeSlots.forEach((element, index) => {
    sqlInsert += `INSERT INTO bookings (user_id, time, date, facility_id) VALUES (${req.session.userId}, ${checkedTimeSlots[index].value}, "${chosenDate}", ${chosenFacility});`;
  });

  connection.query(sqlInsert, (error, results) => {
    if (error) throw error;
    const bookings = results;
  });
});

// delete a booking

router.delete('/bookings/:booking_id', (req, res) => {
  const sqlDelete = 'DELETE FROM bookings WHERE booking_id = ?';

  connection.query(sqlDelete, [req.params.booking_id], (error, results) => {
    if (error) throw error;
    console.log(results);
    return res.send({ data: 'Got a DELETE request at /bookings' });
  });
});

module.exports = router;
