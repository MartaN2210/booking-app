const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('dotenv').config();

const session = require('express-session');
const rateLimiter = require('express-rate-limit');

const server = require('http').createServer(app);

const sessionMiddleware = session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
  },
});

const io = require('socket.io')(server, {
  transports: ['websocket', 'polling'],
});

io.use(function (socket, next) {
  sessionMiddleware(socket.request, socket.request.res || {}, next);
});

const connection = require('./db_connection');

app.use(sessionMiddleware);

app.use(
  rateLimiter({
    windowMs: 10 * 60 * 100, // 10 minutes
    max: 120, // limit each IP to 20 requests per windowMs
  })
);

app.use(
  '/auth',
  rateLimiter({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 130, // limit each IP to 10 requests per windowMs
    handler: function (req, res /* , next */) {
      res.status(429).send({ message: this.message });
    },
  })
);

const authRoutes = require('./routes/auth.js');
const bookingRoutes = require('./routes/bookings.js');
const facilitiesRoutes = require('./routes/facilities.js');
const usersRoutes = require('./routes/users.js');
const messagesRoutes = require('./routes/messages.js');

app.use(authRoutes.router);
app.use(bookingRoutes);
app.use(facilitiesRoutes);
app.use(usersRoutes);
app.use(messagesRoutes);

app.get('/home', authRoutes.requireAuth, (req, res) => {
  const str = `SELECT * FROM users WHERE user_id=?`;

  connection.query(str, [req.session.userId], (error, results) => {
    if (error) throw error;
    const [user] = results;
    if (!user) {
      res.send({ data: 'not found' });
    } else {
      res.send({ data: user });
    }
  });
});

io.on('connection', (socket) => {
  socket.on(
    `priv_notification_${socket.request.session.userId}`,
    ({ data }) => {
      socket.broadcast.emit(`priv_notification_1000`, {
        data: `New message from ${data}`,
      });
    }
  );
});

// app.listen(8080);

server.listen(8080, (error) => {
  console.log(`Server is running on port`, 8080);
});
module.exports = { server, io };
