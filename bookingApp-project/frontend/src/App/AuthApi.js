import React, { useEffect, useState } from 'react';

export const AuthApi = React.createContext();

export const AuthProvider = (props) => {
  const [auth, setAuth] = useState(false);

  useEffect(() => {
    fetch('http://localhost:3000/auth/is_auth')
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setAuth(data);
      });
  }, []);

  return (
    <AuthApi.Provider value={[auth, setAuth]}>
      {props.children}
    </AuthApi.Provider>
  );
};
