import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { AuthApi } from '../../App/AuthApi';
import CalendarForm from '../../components/CalendarForm/CalendarForm';
import './home.css';

const AddBooking = () => {
  const [name, setName] = React.useState(false);

  return (
    <Container className="main-container">
      <Row className="justify-content-center">
        <Col md={10}>
          <CalendarForm />
        </Col>
      </Row>
    </Container>
  );
};

export default AddBooking;
