import React, { useState, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { AuthApi } from '../../App/AuthApi';

import './newMessage.css';

export default function NewMessage() {
  const [auth] = useContext(AuthApi);
  const [subject, setSubject] = useState();
  const [message, setMessage] = useState();
  const [validationMessage, setValidationMessage] = useState();

  const socket = io('http://localhost:8080', {
    transports: ['websocket', 'polling'],
  });

  const handleInputSubjectChanged = (event) => {
    setSubject(event.target.value);
    setValidationMessage('');
  };
  const handleMessageChanged = (event) => {
    setMessage(event.target.value);
    setValidationMessage('');
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();

    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        senderId: auth.user_id,
        recipientId: 1000,
        subject: subject,
        message: message,
        date: new Date().toISOString().split('T')[0],
      }),
    };

    (async () => {
      try {
        const response = await fetch(
          'http://localhost:3000/messages',
          requestOptions
        );
        const data = await response.json();
        console.log(data);
        if (response.ok) {
          setValidationMessage(data.message);
          setMessage('');
          setSubject('');
          socket.emit(`priv_notification_${auth.user_id}`, {
            data: auth.user_email,
          });
        }
      } catch (error) {
        setValidationMessage('Ups! Something went wrong. Please try again');
      }
    })();
  };

  return (
    <Container className="new-message-container">
      <h2>Contact admin</h2>
      <Row>
        <Col>
          <Form onSubmit={handleFormSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Subject</Form.Label>
              <Form.Control
                type="text"
                placeholder="e.g. Broken window"
                onChange={handleInputSubjectChanged}
                value={subject || ''}
                required
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label>Message to admin</Form.Label>
              <Form.Control
                as="textarea"
                rows={6}
                onChange={(event) => handleMessageChanged(event)}
                value={message || ''}
                required
              />
            </Form.Group>
            <p>{validationMessage || ''}</p>
            <Button variant="primary" type="submit">
              Send
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
