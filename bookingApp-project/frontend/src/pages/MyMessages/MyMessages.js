import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { BiMessageSquareAdd } from 'react-icons/bi';
import { IoMdSend } from 'react-icons/io';
import { HiOutlineInboxIn } from 'react-icons/hi';
import Loader from 'react-loader-spinner';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { AuthApi } from '../../App/AuthApi';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import './myMessages.css';

export default function MyMessages() {
  const [auth] = useContext(AuthApi);
  const [messages, setMessages] = useState();
  const [inboxOrSent, setInboxOrSent] = useState('Inbox');

  useEffect(() => {
    (async () => {
      if (inboxOrSent === 'Inbox') {
        const url = `http://localhost:3000/messages/filter?recipient_id=${auth.user_id}`;
        const response = await fetch(url);
        const data = await response.json();
        setMessages(data.data);
      } else {
        const url = `http://localhost:3000/messages/filter?sender_id=${auth.user_id}`;
        const response = await fetch(url);
        const data = await response.json();
        setMessages(data.data);
      }
    })();
  }, [auth.user_id, inboxOrSent]);

  const handleFetchInbox = () => {
    setInboxOrSent('Inbox');
  };

  const handleFetchSent = () => {
    setInboxOrSent('Sent');
  };

  return (
    <Container className="main-container justify-content-start my-messages-content">
      <Row className="mb-3">
        <Col>
          <Button variant="primary" className="d-flex align-items-center">
            <BiMessageSquareAdd color="#386641" size={25} />
            <Link className="ml-3" to="/newmessage">
              New message
            </Link>
          </Button>
        </Col>
      </Row>
      <h2>Messages</h2>
      <Row className="mb-3">
        <Col className="d-flex">
          <Button
            variant="primary"
            onClick={handleFetchInbox}
            className={`d-flex align-items-center mr-2 ${
              inboxOrSent === 'Inbox' ? 'active' : ''
            } `}
          >
            <HiOutlineInboxIn color="#386641" size={20} className="mr-3" />
            Inbox
          </Button>
          <Button
            variant="primary"
            onClick={handleFetchSent}
            className={`d-flex align-items-center mr-2 ${
              inboxOrSent === 'Sent' ? 'active' : ''
            } `}
          >
            <IoMdSend color="#386641" size={20} className="mr-3" />
            Sent
          </Button>
        </Col>
      </Row>
      <Row className="justify-content-center mb-5">
        <Col md={12}>
          {!messages ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <table className="my-bookings-table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>From</th>
                  <th>Subject</th>
                </tr>
              </thead>
              <tbody>
                {messages.map((message) => {
                  return (
                    <tr key={message.message_id}>
                      <td>
                        {new Date(message.date).toLocaleDateString('en-GB')}
                      </td>
                      <td>{message.user_email}</td>
                      <td>{message.subject}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          )}
        </Col>
      </Row>
    </Container>
  );
}
