import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

export default function Messages() {
  const [isLoading, setIsLoading] = useState(true);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    setIsLoading(false);
    loadData();
  }, []);

  const loadData = async () => {
    const api = 'http://localhost:8080/messages';
    const response = await fetch(api);
    const data = await response.json();
    console.log(data);
    setMessages(data);
  };

  return (
    <div className="main container">
      <div className="row">
        <div className="col-sm-2" />
        <div className="col-sm-8">
          <h1>Messagess</h1>
          {isLoading ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>From</th>
                  <th>Subject</th>
                </tr>
              </thead>
              <tbody>
                {messages.map((message) => {
                  return (
                    <tr key={message.message_id}>
                      <td>
                        From sender id is:
                        {message.sender}
                      </td>
                      <td>{message.subject}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          )}
        </div>
        <div className="col-sm-2" />
      </div>
    </div>
  );
}
