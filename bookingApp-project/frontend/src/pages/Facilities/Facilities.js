import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

export default function Facilities() {
  const [isLoading, setIsLoading] = useState(true);
  const [facilities, setFacilities] = useState([]);

  useEffect(() => {
    setIsLoading(false);
    loadData();
  }, []);

  const loadData = async () => {
    const api = 'http://localhost:8080/facilities';
    const response = await fetch(api);
    const data = await response.json();
    setFacilities(data);
  };

  return (
    <div className="main container">
      <div className="row">
        <div className="col-sm-2" />
        <div className="col-sm-8">
          <h1>Facilities</h1>
          {isLoading ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Added date</th>
                  <th>Name</th>
                  <th>Available</th>
                </tr>
              </thead>
              <tbody>
                {facilities.map((facility) => {
                  return (
                    <tr key={facility.facility_id}>
                      <td>---</td>
                      <td>{facility.name}</td>
                      <td>{facility.available}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          )}
        </div>
        <div className="col-sm-2" />
      </div>
    </div>
  );
}
