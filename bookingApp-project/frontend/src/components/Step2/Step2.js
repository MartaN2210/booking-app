import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import './Step2.css';
import { BiTimeFive } from 'react-icons/bi';

export default function Step2({
  chosenDate,
  chosenFacility,
  setCurrentStepNext,
  setCurrentStepBack,
}) {
  const [timeSlots, setTimeSlots] = useState([]);

  useEffect(() => {
    (async () => {
      try {
        const response = await fetch(
          `http://localhost:3000/bookings/filter?from_date=${chosenDate}&to_date=${chosenDate}&facility_id=${chosenFacility}`
        );
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        } else {
          const data = await response.json();
          const items = [];

          for (let i = 8; i < 24; i += 1) {
            items.push({
              value: i,
              timeFrom: i,
              timeTo: i + 1,
              availability: true,
              checked: false,
            });
          }

          for (let i = 0; i < data.data.length; i += 1) {
            const bookedItem = items.find(
              (item) => item.value === data.data[i].time
            );

            bookedItem.availability = false;
            console.log(bookedItem);
          }
          setTimeSlots(items);
        }
      } catch (error) {
        console.error(error);
      }
    })();
  }, []);

  const handleOnChange = (event) => {
    if (event.target.checked) {
      setTimeSlots(
        timeSlots.map((slot) => {
          if (slot.value === Number(event.target.value)) {
            return { ...slot, checked: true };
          }
          return slot;
        })
      );
    } else {
      setTimeSlots(
        timeSlots.map((slot) => {
          if (slot.value === Number(event.target.value)) {
            return { ...slot, checked: false };
          }
          return slot;
        })
      );
    }
    console.log(timeSlots);
  };

  // submit bookings and go next:
  const handleGoNext = () => {
    setCurrentStepNext();
    const checkedTimeSlots = timeSlots.filter((slot) => slot.checked === true);

    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        checkedTimeSlots: checkedTimeSlots,
        chosenDate: chosenDate,
        chosenFacility: chosenFacility,
      }),
    };

    // submit bookings:
    (async () => {
      try {
        const response = await fetch(
          'http://localhost:3000/bookings',
          requestOptions
        );
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        } else {
          setCurrentStepNext();
        }
      } catch (error) {
        console.error(error);
        // setMessage('Ups! Something went wrong. Please try again');
      }
    })();
  };

  const handleGoBack = () => {
    setCurrentStepBack();
  };

  return (
    <Container>
      <Row className="pt-4 mb-3">
        <Col xs={12} className="d-flex align-items-center">
          <BiTimeFive size={20} className="mx-3" />
          <p className="d-inline m-0">
            {`Avaliable times at `}
            <strong>{chosenDate}</strong>
          </p>
        </Col>
      </Row>
      <Row className="step2-container pt-5 mb-5 px-5 align-items-center">
        {timeSlots.map((item) => (
          <Col xs={3} className="mb-1" key={item.value}>
            <label
              htmlFor={item.value}
              className={`text-center timeslot-button ${
                item.availability ? ' ' : 'inactive '
              } ${item.checked ? 'checked' : ' '}`}
            >
              {`${item.timeFrom}:00 - ${item.timeTo}:00`}
            </label>
            <input
              type="checkbox"
              id={item.value}
              value={item.value}
              onChange={(event) => handleOnChange(event)}
            />
          </Col>
        ))}
      </Row>
      <Row className="justify-content-center">
        <Button
          onClick={handleGoBack}
          className="form-button form-button-back m-2"
        >
          Back
        </Button>
        <Button
          onClick={handleGoNext}
          className="form-button form-button-next m-2"
        >
          Finish
        </Button>
      </Row>
    </Container>
  );
}
