import React from 'react';
import Nav from '../Nav/Nav';
import logo from '../../assets/images/logo.png';
import './header.css';

export default function Header() {
  return (
    <header>
      <div className="logoWrap">
        <div>
          <img src={logo} alt="logo" />
        </div>
        <div>
          <p>Green College</p>
          <p>Booking App</p>
        </div>
      </div>
      <Nav />
    </header>
  );
}
