---
layout: post
title: 'Development environment'
date: 2016-03-24 15:32:14 -0300
categories: jekyll update
---

<h3> Visual Studio Code </h3>

In the following project we are using Visual Studio Code as code editor. You can download it from the [VSC website][vsc-website]

Syncing your Visual Studio Code configuration with the one used by the team:

1. In VS Code search for the extension `Settings Sync` in the Extensions panel and install it. Once installed, it will open up a new tab with the Settings Sync dashboard.

2. You need to Authorize access to Github in order to use the plugin. To get started click `Login with Github` button in the Settings Sync dashboard.

3. To download the settings go to the Command Palette and type “sync download” and hit enter which will open up the plugin's dashboard. Here, click `Edit Configuration`

4. Add the following gist id: a404d4c42ec6b34619fac3d693866eff. Once it's there type again “sync download” in the Command Palette. When you hit enter Sync Settings will fetch VS Code configuration for the project.

<h3> Git repository </h3>

Clone the repository from GitLab by using the following command:

{% highlight ruby %}
$ git clone https://gitlab.com/MartaN2210/booking-app.git
{% endhighlight %}

<h3> MySQL </h3>

Make sure you have `MySQL` server installed on your machine. Open terminal and type:

{% highlight ruby %}
$ mysql -v
{% endhighlight %}

If you don't have `MySQL`, go to [MySQL community][mysql-website] website. Download and install the latest community version for your operating system.

You can connect to MySQL server by using `the terminal` or `MySQL Workbench`.

If you prefer to work with `the terminal`. Open it and type following command:

{% highlight ruby %}
$ mysql -u root -p
{% endhighlight %}

If you would like to change the password, you can use the following command:

{% highlight ruby %}
mysql$ ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass';
{% endhighlight %}

To download the `Workbench` go to MySQL website again. This time to the following: [page][mysql-workbench]. Choose the correct version for your operating system.

Once the Workbench is installed, you can launch it and interact with the user interface.

<h3> Node.js </h3>

Make sure you have `Node.js` installed on your machine. Open terminal and type:

{% highlight ruby %}
$ node -v
{% endhighlight %}

If you don't have `Node.js` or you have an old version, go to [Node.js][node-website] website. Download and install the latest version for your operating system.

<h3> NPM </h3>

When you download Node.js, you automatically get npm installed on your machine, but NPM tends to update more frequently. It's always a good idea to upload the latest version. You can do that with following command:

{% highlight ruby %}
$ npm install npm@latest -g
{% endhighlight %}

<h3> Node modules </h3>

To istall project moduls run following commands:

For frontend:
{% highlight ruby %}
$ cd bookingApp-project/frontend npm ic
{% endhighlight %}

For backend:
{% highlight ruby %}
$ cd bookingApp-project/backend npm ic
{% endhighlight %}

[node-website]: https://nodejs.org/en/
[mysql-website]: https://www.mysql.com/
[mysql-workbench]: https://dev.mysql.com/downloads/workbench/
[vsc-website]: https://code.visualstudio.com/
